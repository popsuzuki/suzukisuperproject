class Klass
  def test_method
    public_method
    private_method
  end

  def public_method
    puts "public_method"
  end

  private
  def private_method
    puts "private_method"
  end
end

klass = Klass.new
klass.test_method #=> public_method
                  #=> private_method