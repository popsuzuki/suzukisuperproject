class Foo
  def foo
   p caller.last
  end
  protected :foo
end

obj = Foo.new

# そのままでは呼べない
obj.foo rescue nil    # => NoMethodError

# クラス定義内でも呼べない
class Foo
  Foo.new.foo rescue nil # => NoMethodError
  # メソッド定義式内で呼べる
  def bar
    self.foo
  end
end
Foo.new.bar             # => ["-:21"]

# 特異メソッド定義式内でも呼べる
def obj.bar
  self.foo rescue nil
end
obj.bar                 # => ["-:27"]