class Klass
  def method_a
    puts "method_a"
  end
  def method_b
    puts "method_b"
  end

  private
  def method_c
    puts "method_c"
  end
  def method_d
    puts "method_d"
  end
end


klass = Klass.new
klass.method_a #=> methoc_a
klass.method_c #=> NoMethodError