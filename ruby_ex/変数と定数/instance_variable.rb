# インスタンス変数
# インスタンス変数は、先頭を@にすることで定義できます
# インスタンス変数はインスタンス（オブジェクト）毎に別の値を持つことができる変数です。
# 通常、各オブジェクトの状態を表すために使用されます。
class User
  def initialize(id, age, blood_type)
    @id = id
    @age = age
    @blood_type = blood_type
  end
  attr_accessor :id, :age, :blood_type
  def to_s
    return "ID:#{@id}, 年齢:#{@age}, 血液型:#{@blood_type}"
  end
end
first_user = User.new("0001", 20, "A")
p first_user.to_s # => "ID:0001, 年齢:20, 血液型:A"
second_user = User.new("0002", 25, "B")
p second_user.to_s # => "ID:0002, 年齢:25, 血液型:B"
# first_userとsecond_userは別のインスタンスなのでインスタンス変数の中身は別の値となっている。
# second_userの年齢を一つ加算しても、first_userは変更しない
second_user.age += 1
p first_user.to_s # => "ID:0001, 年齢:20, 血液型:A"
p second_user.to_s # => "ID:0002, 年齢:26, 血液型:B"
