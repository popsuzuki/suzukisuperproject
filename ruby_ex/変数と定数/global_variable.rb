#グローバル変数
# グローバル変数は、先頭を$にすることで定義できます
# グローバル変数はどこからでもアクセスすることができます。
class MyClass
  $global_variable = "global_variable!"
  def my_method
    p $global_variable
  end
  p $global_variable # => "global_variable!"
end
p $global_variable # => "global_variable!"
MyClass.new.my_method # => "global_variable!"
