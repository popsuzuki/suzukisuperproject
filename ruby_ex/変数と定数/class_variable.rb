# クラス変数
# クラス変数は、先頭を@@にすることで定義できます
# クラス変数はそのクラスの全てのインスタンスで共有される変数です。
class Programmer
  @@ruby = 0
  @@java = 0
  @@c = 0
  def initialize(name, age, language)
    @name = name
    @age = age
    @language = language
    case language.downcase
    when "ruby"
      @@ruby += 1
    when "java"
      @@java += 1
    when "c"
      @@c += 1
    end
  end
  def self.favorite_language
    p "ruby=#{@@ruby}, java=#{@@java}, c=#{@@c}"
  end
end
Programmer.new("Alice", 20, "c")
Programmer.new("Bob", 21, "Ruby")
Programmer.new("Carol", 22, "java")
Programmer.new("Dave", 23, "Ruby")
Programmer.new("Emma", 24, "java")
Programmer.new("Frank", 25, "c")
Programmer.new("Grace", 26, "Ruby")
Programmer.new("Harry", 27, "Ruby")
Programmer.new("Isabel", 28, "java")
Programmer.new("John", 29, "Ruby")
Programmer.favorite_language # => "ruby=5, java=3, c=2"
