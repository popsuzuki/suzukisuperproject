# ローカル変数
# ローカル変数は、先頭をアルファベット小文字または`_'で定義できます
# ローカル変数のスコープは、その変数が宣言されたブロック、メソッド定義、またはクラス/モジュール定義の終りまでです。
# my_methodで定義されたlocal_variableはother_methodからは参照できません
class MyClass
  def my_method
    local_variable = "my_method"
    p local_variable
  end
  def other_method
    p local_variable 
  end
end
MyClass.new.my_method # => "my_method"
MyClass.new.other_method # => (NameError)
