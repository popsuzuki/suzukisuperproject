# ローカル変数
# ローカル変数は、先頭をアルファベット小文字または`_'で定義できます
# ローカル変数のスコープは、その変数が宣言されたブロック、メソッド定義、またはクラス/モジュール定義の終りまでです。
# MyClassで定義されたlocal_variableはMyClassの外から参照できません。
class MyClass
  local_variable = "my_class"
  puts local_variable # => "my_class"
end
puts local_variable # => (NameError)