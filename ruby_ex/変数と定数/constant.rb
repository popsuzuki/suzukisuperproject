# 定数
# 定数は、先頭をアルファベット大文字にし、定数に代入することで定義(と初期化)ができます
# また、メソッドの 中では定義できません。
# 一度定義された定数に再び代入を行おうと すると警告メッセージが出ます
class ConstClass
  CONSTANT = "constClass"
  VERSION = "1.0.0"
  CONSTANT = "variable" # => warning(警告メッセージ)
end
# あるクラスまたはモジュールで定義された定数を外部から参照するには`::'演算子を用います。
module ConstModule
  CONSTANT = "constModule"
  VERSION = "1.0.0"
end
p ConstModule::CONSTANT # => constModule
