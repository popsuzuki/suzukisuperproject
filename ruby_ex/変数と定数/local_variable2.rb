# ローカル変数
# ローカル変数は、先頭をアルファベット小文字または`_'で定義できます
# ローカル変数のスコープは、その変数が宣言されたブロック、メソッド定義、またはクラス/モジュール定義の終りまでです。
# MyClassで定義されたlocal_variableはmy_methodからは参照できません
class MyClass
  local_variable = "my_class"
  def my_method
    p local_variable
  end
  puts local_variable # => "my_class"
end
MyClass.new.my_method # => (NameError)
