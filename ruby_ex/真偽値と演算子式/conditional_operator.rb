#if 条件式 then
#  a = b
#else
#  a = c
#end
#
# a = 条件式 ? b : c
score = 80
a = score >= 75 ? "合格" : "不合格"
puts a #=> "合格"
