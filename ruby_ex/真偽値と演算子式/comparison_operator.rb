# 年齢が65歳以上もしくは、血液型がB型の場合。
age = 30
blood_type = "B"
if age >= 65 || blood_type == "B" then
  puts "match!" #=> "match!"
end

# 未成年且つ、血液型がAB型以外且つ、学生でない場合。
age = 19
blood_type = "O"
is_student = false
if age < 20 && blood_type != "AB" && !is_student then
  puts "match!!" #=> "match!!"
end
