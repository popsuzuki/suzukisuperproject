#ブロック付きメソッドの定義
# ブロック付きメソッドを自分で定義するには yield 式を使います。
# yield が受け取ったブロック部分を実行するメソッドです。
def my_method(x, y)
  x + yield(y, 10)
end
p my_method(1, 2) {|a, b| a + b} #=> 1 + 2 + 10 = 13
p my_method(1, 2) {|a, b| a * b} #=> 1 + 2 * 10 = 21
