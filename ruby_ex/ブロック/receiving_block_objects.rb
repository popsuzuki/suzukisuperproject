#ブロックをオブジェクト(Procクラスのインスタンス)として受け取る事もできます。
# &引数名にすると与えられたブロックはProcオブジェクト化される
def my_method(x, y, &block)
  x + block.call(y, 10)
end
p my_method(1, 2) {|a, b| a + b} #=> 1 + 2 + 10 = 13
p my_method(1, 2) {|a, b| a * b} #=> 1 + 2 * 10 = 21
