# サンプルとしてパラメータfrom からto までの数値の合計を算出するブロック付きメソッドの例
def calculate(from, to)
  result = from
  from += 1
  from.upto(to) do |num|
    result = yield(result, num)
  end
  return result
end

p calculate(1, 10) {|total, num| total + num} #=> 55
p calculate(1, 10) {|total, num| total * num} #=> 3628800
p calculate(1, 100) {|total, num| total + num} #=> 5050
