# Procを利用して、ブロックを事前にオブジェクト化しておくこともできます
def my_method(x, y, block)
  x + block.call(y, 10)
end
proc = Proc.new do |a, b|
  a * b
end
p my_method(1, 5, proc) #=> 1 + 5 * 10 = 51
p my_method(1, 6, proc) #=> 1 + 6 * 10 = 61
