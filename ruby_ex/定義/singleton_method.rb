# 特異メソッドの定義
# 特異メソッドはクラスではなく特定のオブジェクトに、定義されたメソッドです
# 以下例では、aにのみ特異メソッドgreetingを定義しています。
class Person
end
a = Person.new
b = Person.new
def a.jump
  puts "nice jump!"
end
a.jump # => "nice jump!"
b.jump # => 例外発生(NoMethodError)
