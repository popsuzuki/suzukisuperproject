# クラスの継承
# Personクラスを継承したStudentクラスはPersonクラスの持つメソッドを利用することができます。
class Person
  def greeting
    puts "hello!"
  end
end

class Student < Person
end
student = Student.new
student.greeting #=> "hello!"
