# メソッドの戻り値を設定する
# メソッド内でreturnを呼び出し、引数に戻り値となる値を指定します
# returnを省略した場合、最後に実行された行の評価値が戻り値となります
class Person
# インスタンスの生成（オブジェクトの初期化）initializeのオーバーライド
  def initialize(country, city)
    @country = country
    @city = city
  end
  def country
    return @country
  end
  def city
    @city
  end
end
person = Person.new("Japan", "Tokyo") # newメソッドを呼び出した際にinitializeメソッドが呼び出される。
p person.country # => Japan
p person.city # => Tokyo
