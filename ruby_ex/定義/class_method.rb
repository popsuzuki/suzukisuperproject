# クラスメソッドを定義する
# クラスメソッドはインスタンスを生成せずに呼び出すことができます。
# self.メソッド名、またはクラス名.メソッド名で定義します。
class Person
  def self.number_of_cells
    puts "約37兆個"
  end
#  def Person.number_of_cells
#    puts 37,000,000,000,000
#  end
end
Person.number_of_cells
