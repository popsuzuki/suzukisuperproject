#メソッドの仮引数でデフォルト値を指定することができます
#仮引数nameに"Lisa"を指定することにより、メソッド呼出時に実引数を省略できます
class Person
  def greeting(name = "Lisa")
    p "hello #{name}"
  end
end
person = Person.new
person.greeting # => "hello Lisa"
