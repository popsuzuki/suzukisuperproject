#メソッド定義
#メソッド名は基本的にアルファベットの小文字で定義します。
#メソッドで値を受け取るにはメソッド名の後に括弧「()」で仮引数を指定する
#以下例ではnameとageが‘仮引数’となります。
#またメソッドに値を渡す側は‘実引数’と呼ばれる。以下例では("Ken", 20)
class Person
  def greeting(name, age)
    p "hello #{name}"
  end
end
person = Person.new
person.greeting("Ken", 20) # => "hello Ken"
