# 特異クラスの定義
# 特異メソッドを纏めて定義できます。
class Person
end
a = Person.new
class << a
  def walk
    puts "slowly"
  end
  def run
    puts "faster"
  end
end
a.walk # => "slowly"
a.run # => "faster"
