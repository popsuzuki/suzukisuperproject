#クラス定義
# クラス名はアルファベットの先頭の文字は大文字にする必要があります。
class Person
  def greeting
    puts "hello!"
  end
end
person = Person.new
person.greeting #=> "hello!"
