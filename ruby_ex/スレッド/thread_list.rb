#実行中のスレッド一覧を取得する
#Thread::listメソッドにより実行中スレッドの一覧を配列で取得することができます。
#以下の例はメインスレッドから生成した2つのスレッドを、 Thread::killメソッドにより停止させます。
#Thread::listはメインスレッドも返却しますので、Thread::currentメソッドによりメインスレッドを取得し、
#それ以外にメソッドに対してのみThread::killメソッドを発行しています。
puts "start"

thread1 = Thread.new do
  puts "start thread 1"
  sleep 10
  puts "end thread 1"
end

thread2 = Thread.new do
  puts "start thread 2"
  sleep 10
  puts "end thread 2"
end

Thread::list.each {|t| Thread::kill(t) if t != Thread::current}

p thread1.join #=> <Thread:0x00000000 dead>
p thread2.join #=> <Thread:0x00000000 dead>

puts "end"
