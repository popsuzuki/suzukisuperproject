#スレッドの実行を終了させる
#Thread::killメソッドによりメインスレッドからスレッドの実行を終了させることができます。
puts "start"

thread = Thread.new do
  puts "start thread"
  count = 10
  10.times do
    puts count
    count -= 1
    sleep 1
  end
  puts "end thread"
end

while line = gets
  if line.chop == "." then
    Thread::kill(thread)
    puts "the thread killed"
    break
  end
end

puts "end"
#上記例では「.」をキーボードから入力すると生成したスレッドの実行を終了し、
#メッセージを出力した後、メインスレッドも終了します。
