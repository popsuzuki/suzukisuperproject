#スレッドに引数を渡す
#引数を渡してスレッドを生成するにはスレッド生成のためのメソッドThread#new、Thread#start、Thread#forkに対して引数を指定します。
#引数はスレッドのためのブロックへそのまま渡されます。
puts "start"

thread = Thread.new("Dog", 3) do |param1, param2|
  puts "start thread - #{param1} #{param2.to_s}" #=> "start thread - Dog 3"
  sleep 3
  puts "end thread"
end

puts "waiting for the thread to complete"
thread.join
puts "end"
