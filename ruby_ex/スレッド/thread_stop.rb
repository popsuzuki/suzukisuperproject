#スレッドを停止する
#Thread::stopメソッドによりカレントスレッドの動作を停止することができます。
#停止したスレッドは他のスレッドからThread#runメソッドにより起動されるまで、停止しています。
puts "start"

thread = Thread.new do
  puts "start thread"
  Thread.stop
  puts "end thread"
end

gets
thread.run
thread.join
puts "end"
#上記の例では生成したスレッドは"start thread"というメッセージを出力後、Thread#stopにより停止します。
#メインスレッドではgetsにより標準入力からの入力待ちに入り、改行を入力するとThread#runにより停止したスレッドを再開させます。
