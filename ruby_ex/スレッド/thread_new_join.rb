#スレッドを生成する
#Thread::new、Thread::start、Thread::forkメソッドにより新しくスレッドを生成することができます。

#スレッドの終了を待つ
#生成したスレッドの終了を待つにはThread#joinメソッドを使用します。

puts "start main"

thread = Thread.new do # ここでスレッド作成
  puts "start thread" # ここ（do～end）はthreadが実施する
  sleep 1
  puts "end thread"
end

puts "waiting for the thread to complete" # メインスレッドはここから実行を継続
thread.join # threadが完了するまで待機
puts "end main" # threadが完了後に継続

#=> start main
#=> start thread
#=> waiting for the thread to complete
#=> end thread
#=> end main
