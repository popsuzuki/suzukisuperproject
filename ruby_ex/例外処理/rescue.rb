# rescue 例外の補足
# ensure 後処理
score = 0
begin
  puts 100 / score # 0除算によりZeroDivisionErrorが発生
rescue => ex 
  puts "例外を補足しました"
  puts ex.message
  puts ex.class
ensure
  puts "後処理を実施"
end
