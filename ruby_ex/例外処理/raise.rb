# 例外クラスの自作
# StandardErrorを継承して定義する。
class MyError < StandardError
end

# raise 例外を発生させる
# rescue は複数記述することができる。
# また補足する例外を指定することができる。
score = 50
begin
  if score == 50 then
    raise MyError
  end
  puts 100 / score # 0除算によりZeroDivisionErrorが発生
rescue MyError => ex 
  puts "MyErrorを補足しました"
rescue ZeroDivisionError => ex 
  puts "ZeroDivisionErrorを補足しました"
rescue => ex 
  puts "その他の例外を補足しました"
  puts ex.message
  puts ex.class
ensure
  puts "後処理を実施"
end