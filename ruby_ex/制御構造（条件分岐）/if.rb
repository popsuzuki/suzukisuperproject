# ifは、ifの条件式が真であるときにifの処理を実行します。
age = 70
if age >= 65 then
  print "senior citizen" #=> "senior citizen"
end