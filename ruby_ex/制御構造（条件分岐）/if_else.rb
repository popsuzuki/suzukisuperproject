# elseは全ての if および elsif の条件式が偽であったときにelseの処理を実行します。
age = 10
if age >= 65 then
  print "senior citizen"
elsif age >= 20 && age < 65 then
  print "adult"
else
  print "minor" #=> "minor"
end