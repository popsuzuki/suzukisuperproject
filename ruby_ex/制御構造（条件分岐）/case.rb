# case は一つの式に対する一致判定による分岐を行います
# when節は複数指定することができます。
animal = "dog"
case animal
when "dog" then
  puts "bow-wow" #=> "bow-wow"
when "cat" then
  puts "meow"
when "lion", "bear" then
  puts "roar"
else
  puts "..."
end