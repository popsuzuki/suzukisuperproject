# elsifは、ifの条件式が偽で、且つelsifの条件式が真であるときにelsifの処理を実行します。
# またelsifは複数指定することができ、複数指定した場合には直前までのifおよびelsifの条件式が偽であった場合に評価します。
age = 18
if age >= 65 then
  print "senior citizen"
elsif age >= 20 && age < 65 then
  print "adult"
elsif age >= 13 && age < 20 then
  print "teenager" #=> "teenager"
end