#例えば以下のような論理否定演算子(!)を使用した条件式のifを記述する場合には、unlessを使用することが推奨されています。
#flag = false
#if !flag then
#  puts "flag off" #=> "flag off"
#end

flag = false
unless flag then
  puts "flag off" #=> "flag off"
end
