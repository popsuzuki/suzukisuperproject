# -*- coding: utf-8 -*-
require 'rdbi'       # RDBIを使う
require 'rdbi-driver-sqlite3' # RDBIでSQLite3と接続するドライバ

# データベース'fruits01.db'に接続する
dbh = RDBI.connect( :SQLite3, :database => "fruits01.db" )

# テーブルに登録されたデータを削除する
# delete文の実行
# sthにexecuteメソッドが返すステートメントハンドルを保持
sth = dbh.execute("delete from products")
puts "all records are deleted."
 
# ステートメントハンドルを解放する
sth.finish

# サーバから切断
dbh.disconnect
