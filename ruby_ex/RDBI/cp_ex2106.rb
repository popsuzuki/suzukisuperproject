# -*- coding: utf-8 -*-
require 'rdbi'       # RDBIを使う
require 'rdbi-driver-sqlite3' # RDBIでSQLite3と接続するドライバ

# dbhを作成し、データベース'students01.db'に接続する
# 第一パラメータはDBDドライバ名を指定します。(今回はSQLite3を使用します)
# 第二パラメータはデータベースファイル名を指定しています。
#   (HashでkeyにSymbolで:database、valueにはDBファイル名を文字列で指定)
# connectメソッドの戻り値にはRDBI::Databaseのインスタンスを返します。
dbh = RDBI.connect( :SQLite3, :database => "students01.db" )
                    #ハッシュの生成
# select文でデータを検索し、読み込んだデータを表示する
dbh.execute("select * from students").fetch(:all).each do | row |
  # SQL「select * from students」を実行し、
  # 1行ずつをrowで受け取ってブロックを処理する
  print "row=#{row}           \n"
  print "  name = #{row[0]}\n"
  print "  age  = #{row[1]}\n"
  print "\n"
end

# データベースとの接続を終了する
dbh.disconnect
