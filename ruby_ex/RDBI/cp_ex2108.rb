# -*- coding: utf-8 -*-
require 'rdbi'       # RDBIを使う
require 'rdbi-driver-sqlite3' #必要i

# データベース'fruits01.db'に接続する
dbh = RDBI.connect( :SQLite3, :database => "fruits01.db" )

# テーブルにデータを追加する
dbh.execute("insert into products values (
    1,
    'りんご',
    '別名「雪の下」とよばれる国光です。',
    '/images/kokkou.jpg',
    '300',
    '2009-02-01 09:15:00'
);")

dbh.execute("insert into products values(
    2,
    'マンゴー',
    '宮崎名産「太陽のたまご」です。',
    '/images/mango.jpg',
    '2000',
    '2009-03-20 00:00:00'
);")

puts "2 records inserted."

# データベースとの接続を終了する
dbh.disconnect
