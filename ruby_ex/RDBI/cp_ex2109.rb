# -*- coding: utf-8 -*-
require 'rdbi'       # RDBIを使う
require 'rdbi-driver-sqlite3' # RDBIでSQLite3と接続するドライバ

# データベース'fruits01.db'に接続する
dbh = RDBI.connect( :SQLite3, :database => "fruits01.db" )

# テーブルからデータを読み込んで表示する
# select文の実行
sth = dbh.execute("select * from products")
# テーブルの項目名を配列で取得する
columns = sth.schema.columns.to_a

# select文の実行結果を1件ずつrowに取り出し、繰り返し処理する
sth.each do |row|
  # rowは1件分のデータを保持している
  col_num = 0
  row.each do |val|
    puts "#{columns[col_num].name}: #{val.to_s}"
    col_num += 1
  end
  puts "----------"
end

# 実行結果を解放する
sth.finish

# データベースとの接続を終了する
dbh.disconnect
