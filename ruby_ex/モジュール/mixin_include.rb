#Mix-in
#Mix-inを利用することで、moduleで提供されているメソッドを使えるようになります。
#Mix-inはクラスの継承と似ていますが、
#Rubyでは同時に複数のクラスを継承する事（多重継承）が出来ませんが
#Mix-inを利用することで複数のモジュールを同時にクラスに取り込む事が出来ます。
# include
#　Moduleを取り込むにはincludeを使用します。

module Logger
  def debug
    puts "#{self.class} debug log"
  end
end
class Player
  include Logger
end
class Manager
  include Logger
end
player = Player.new
player.debug #=> "Player debug log"
manager = Manager.new
manager.debug #=> "Manager debug log"