# extend
#また、includeではインスタンスメソッドとして取り込みを行いましたが、
#extendを使うことでクラスメソッド(クラスの特異メソッド)として取り込むことも出来ます。

module Logger
  def debug
    puts "#{self.class} debug log"
  end
end
class Player
  extend Logger
end
Player.debug  #=> "Player debug log"
