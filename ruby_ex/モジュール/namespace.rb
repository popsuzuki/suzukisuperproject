#名前空間の提供
#名前空間とは、メソッドや定数、クラスの名前を区別して管理する単位のことです。
#異なる名前空間を利用すれば同じ名前のメソッドやクラス同士の衝突を回避する事が出来ます。
#　例えばフレームワーク(Rails等)を利用する場合や、
#　新しいgem(機能を纏めたライブラリのようなもの)を作成する場合に、
#　outputというメソッドを作りたいと思っても、outputというメソッドは既存のgemで
#　提供済みの可能性があります。
#　その場合に、名前空間を使うことで別の物として扱うことができるようになります。

class Dog
  def bark
    puts "bowwow!!"
  end
end
# モジュール名、アルファベットの先頭の文字は(クラス名同様に)大文字にする必要があります。
module Zoo
  NOTES = "Please do not enter the cage."
  class Dog
    def bark
      puts "bowwow!!"
    end
  end
end
# module名とclass名は定数なので、定数の中から定数を呼ぶ場合はコロンを二個(::)つけて呼び出す。
Dog.new.bark #=> "bowwow!!"
Zoo::Dog.new.bark #=> "bowwow!!"
Zoo::NOTES #=> "Please do not enter the cage."
