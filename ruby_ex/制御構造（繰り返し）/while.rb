# 条件式が真である間、本体(do～end)を繰り返し実行します。
i = 0
while i < 10 do
  puts "#{i}: hello world" #=> "0: hello world"
end
