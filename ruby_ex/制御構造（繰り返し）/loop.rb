# loopメソッド
# loop は無限ループを行います。
# loop は繰り返し条件を指定できないので、
# 一般的には処理の中でbreak等を呼び出し、終了条件を指定する
i = 0
loop do
  puts i
  i += 1
  # if i > 100 then
  #   break
  # end
end
