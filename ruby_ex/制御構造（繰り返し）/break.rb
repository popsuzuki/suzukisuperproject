# break
# break はもっとも内側のループを脱出します。
10.times do |i|
  if i == 5 then
    break
  end
  puts "#{i}: hello world"
end
