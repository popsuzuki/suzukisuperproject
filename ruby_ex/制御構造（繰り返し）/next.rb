# next
# nextはもっとも内側のループの次の繰り返しにジャンプします。
10.times do |i|
  if i == 5 then
    next
  end
  puts "#{i}: hello world"
end
