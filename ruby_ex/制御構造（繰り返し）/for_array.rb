# for は内部的にはeachを読んでいますので、上記 eachメソッドの例と同じ処理を以下のように記述できます。
numbers = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"]
for number in numbers do
  puts number
end
