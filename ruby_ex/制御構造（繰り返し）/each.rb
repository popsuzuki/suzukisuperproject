# eachメソッド
# eachメソッド はオブジェクトに含まれている要素の数だけ繰り返します。
numbers = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"]
numbers.each do |number|
  puts number
end
