# while にも if 同様、while修飾子がある
while修飾子の構文
# 処理 while 条件式
# while修飾子の特殊な使い方として左辺の処理がbegin節である場合、一回処理を実行してから条件式を評価します。
i = 20
begin
  puts "#{i}: hello world"
  i += 1
end while i < 10
